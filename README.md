# Communication
[Timely updates to your team](https://gitlab.com/smarter-codes/guidelines/software-engineering/communication/-/issues/1)

# Inspirations
* Inside the book [Building a Career in Software](https://www.oreilly.com/library/view/building-a-career/9781484261477/) > Part III. Communication 
* [How to ask a question in Stackoverflow](https://stackoverflow.com/help/how-to-ask)
